#!/bin/bash
echo "$PATH"|grep -q '/root/bin' || PATH=$PATH:/root/bin

#get all our config vars
. /root/bin/getconfig

#who am I?
NAME="$1"

function network(){
        HOSTNAME=$NAME.$DOMAIN

	#Make sure that we don't have multiple servers making conflicting changes to DNS
	CMDS=$(cat<<EOF
	while [ -f /tmp/dns_in_use.lock ]; do
		sleep 2s
		echo "Waiting on file lock with DNS server........"
	done
	echo "$(hostname -i)" >/tmp/dns_in_use.lock
	cat /etc/hosts.dnsmasq >/etc/hosts.dnsmasq.bk
EOF
);

	ssh -t root@$DNS_SERVER -i $DNS_KEY "$CMDS"

	#Get a copy of the current hosts file and make a backup on server
	echo "Getting host file from DNS server ........"
	ssh -t root@${DNS_SERVER} -i $DNS_KEY 'cat /etc/hosts.dnsmasq' >$HOSTS_FILE

	#Get an unused address and make an entry in the hosts file
	echo "Adding entry to hosts file........"
	for IP in $(nmap -nsL $NETWORK|awk '{print $5}'|egrep -v [a-zA-Z]|sed -n "/^$LOW$/,/^$HIGH$/p"); do
		if ! grep -q $IP $HOSTS_FILE; then
			sed -ri '/^([0-9]{1,3}\.){3}[0-9]{1,3}\s'"$HOSTNAME"'\s*\$/d' $HOSTS_FILE
			echo "$IP $HOSTNAME" >>$HOSTS_FILE
			break
		fi
	done
	ADDRESS=$IP

	#Update the DNS server's hosts file
	HOSTS="$(cat $HOSTS_FILE)"

	CMDS=$(cat<<EOF
		rm -f /tmp/dns_in_use.lock
		echo "$HOSTS" >/etc/hosts.dnsmasq
		service dnsmasq restart
		/root/bin/syncdns
EOF
);

	echo "Updating DNS server........"
	ssh -t root@${DNS_SERVER} -i $DNS_KEY "$CMDS"
	echo "DNS has been updated........"

	NETMASK="$(ipcalc ${IP}/$(echo $NETWORK|cut -d/ -f2)|grep Netmask|awk '{print $2}')"
	BROADCAST="$(ipcalc ${IP}/$(echo $NETWORK|cut -d/ -f2)|grep Broadcast|awk '{print $2}')"

	echo "$HOSTNAME 'A' record set to $ADDRESS in DNS........"
	echo "Broadcast address is $BROADCAST ........"
	echo "Netmask is $NETMASK ........"
}

network
